angular.module('angular-tag-manager', []).factory('$tagManager', ['$q', function ($q) {
        return {
            send: function (key, params) {
                var q = $q.defer();
                var response = dataLayer.push({
                    'event': key,
                    'params': params
                });
                q.resolve(response);
                return q.promise;
            }
        };
    }]);