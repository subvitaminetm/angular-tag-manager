# Angular Tag Manager #

An Angular.js factory for send a dataLayer to Google Tag Manager

Dependencies:

* Angular

### Install with Bower ###

```html
sudo bower install https://bitbucket.org/busineyss/angular-tag-manager.git --save --allow-root
```

In your html/template add 

```html
<script src="/bower_components/angular-tag-manager/dist/angular-tag-manager.js"></script>
```

In your application, declare dependency injection like so:

```javascript
angular.module('myApp', ['angular-tag-manager']);
```

#### Examples ####

```javascript
module.controller('MyCtrl', function($scope, $tagManager) {

  $tagManager.send('myEvent', {
     params1 : 'value1',
     params2 : 'value2',
     params3 : 'value3'
  }).then();

});
```